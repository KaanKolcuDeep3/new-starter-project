// Load express module.
const express = require("express");

// Create an express application
const app = express();

// Load Module - Logging package for nodeJS (allows logging of request information to the console - next function)
const morgan = require("morgan");

// Load Module - Can be used to parse the body of an incoming request.
const bodyParser = require("body-parser");

// import product routes from product's exported router.
const productRoutes = require("./api/routes/products");

// import order routes from order's exported router.
const orderRoutes = require("./api/routes/orders");

// --- middleware ---
// 'dev' is the format used for the output.
app.use(morgan("dev"));
// For URL encoded bodies - without extended enriched data (false)
app.use(bodyParser.urlencoded({ extended: false }));
// Extracts JSON data.
app.use(bodyParser.json());

// Work-around to avoid CORS errors. Second parameter (*) means access to any client to the RESTful API.
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );

  // Browser will always send an options request when a POST or PUT request is made. Browser must check if it is allowed to make such a request.
  if (req.method === "OPTIONS") {
    // Tell the browser what it is allowed to send.
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

// Routes which should handle requests
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);

// Catch all errors that make it past the requests. Handle all requests from here onwards.
// Reaching past above routes means no route was able to handle the request.
app.use((req, res, next) => {
  const error = new Error("Not Found");
  error.status(404);
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
