// Load HTTP module
const http = require("http");

// Import app.js
const app = require("./app");

// Set port to be the environment's port or port 3000 by default.
const port = process.env.PORT || 3000;

// Create HTTP server. The listener passed in 'app' has been imported.
const server = http.createServer(app);

// Server starts listening on specified port (3000)
server.listen(port);