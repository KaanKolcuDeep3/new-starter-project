const express = require("express");
const router = express.Router();

// Handle incoming GET requests to /orders
router.get("/", (req, res, next) => {
  res.status(200).json({
    message: "Orders were fetched"
  });
});

// Handle incoming POST requests to /orders
router.post("/", (req, res, next) => {
  const order = {
    productID: req.body.productID,
    quantity: req.body.quantity
  };
  res.status(201).json({
    message: "Orders was created",
    order: order
  });
});

// Handle incoming get requests for specific orders where a request specified an orderID as a parameter.
router.get("/:orderID", (req, res, next) => {
  res.status(200).json({
    message: "Order details",
    orderID: req.params.orderID
  });
});

// Handle incoming delete requests on the order for which the orderID has been specified within the request.
router.delete("/:orderID", (req, res, next) => {
  res.status(200).json({
    message: "Order deleted",
    orderID: req.params.orderID
  });
});

module.exports = router;